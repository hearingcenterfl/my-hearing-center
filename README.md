My Hearing Center is your center for better hearing in Jensen and West Palm Beach. A full array of professional audiology services is available for seasonal and full-time residents. From hearing evaluations to cleaning and repairing hearing aids, we meet your needs conveniently in your neighborhood.

Address: 2905 N Military Trail, Suite G, West Palm Beach, FL 33409, USA

Phone: 561-689-0160

Website: https://myhearingcenter.net
